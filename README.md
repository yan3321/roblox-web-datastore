# roblox-web-datastore
Web-based datastore for Roblox for storing and fetching JSON strings

## Important Notice
This datastore was made when I was relatively new to backend development, based off gsck's efforts.
You should not be storing JSON data as JSON strings (although it is done here) in SQL or any other relational database.
MongoDB is a better database solution.

## Requirements
### Server
- Node.JS
- MySQL/MariaDB
### Roblox
- Roblox Lua
